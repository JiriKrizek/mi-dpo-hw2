package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JiriKrizek
 */
public class UnaryOperator extends Operator {

    @Override
    public Integer evaluate() {
        return ((NumericOperand) firstOperand).getValue();
    }

    public UnaryOperator(NumericOperand numeric) {
        super(numeric, null);
    }

    @Override
    public ExpressionElement getExpElement() {
        return new cvut.fit.dpo.arithmetic.elements.Number(this.evaluate());
    }

    @Override
    public Object getFirstOperand() {
        return null;
    }

    @Override
    public Object getSecondOperand() {
        return null;
    }

    @Override
    public List<Component> getOpBracket() {
        return new ArrayList<Component>();
    }

    @Override
    public List<Component> getClBracket() {
        return new ArrayList<Component>();
    }
}
