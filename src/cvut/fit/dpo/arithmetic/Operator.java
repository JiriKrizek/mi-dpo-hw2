package cvut.fit.dpo.arithmetic;

/**
 *
 * @author JiriKrizek
 */
public abstract class Operator extends Component {

    public abstract Integer evaluate();

    public Operator(Object firstOperand, Object secondOperand) {
        setFirstOperand(firstOperand);
        setSecondOperand(secondOperand);
    }
}
