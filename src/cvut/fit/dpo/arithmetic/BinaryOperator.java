package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.iterator.InOrderIterator;
import cvut.fit.dpo.arithmetic.iterator.PostOrderIterator;

/**
 * Represents the Binary operations like + - etc.
 * 
 * @author Jan Kurš
 * 
 * Composite
 *
 */
public abstract class BinaryOperator extends Operator {

    protected abstract Integer evaluate(Integer val1, Integer val2);

    public BinaryOperator(Object firstOperand, Object secondOperand) {
        super(firstOperand, secondOperand);
    }

    @Override
    public Integer evaluate() {
        int val1 = getOperandValue(firstOperand);
        int val2 = getOperandValue(secondOperand);

        return evaluate(val1, val2);
    }

    public InOrderIterator inOrderIterator() {
        return null;
    }

    public PostOrderIterator postOrderIterator() {
        return null;
    }

    private Integer getOperandValue(Object o) {
        if (o instanceof NumericOperand) {
            return ((NumericOperand) o).getValue();
        }

        if (o instanceof BinaryOperator) {
            return ((BinaryOperator) o).evaluate();
        }

        throw new IllegalArgumentException("Unsupported operand type!");
    }
}
