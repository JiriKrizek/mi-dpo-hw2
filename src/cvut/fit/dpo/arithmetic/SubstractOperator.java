package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import cvut.fit.dpo.arithmetic.elements.SubstractOperation;

/**
 * Represents - operation
 * 
 * Composite: Composite
 * @author Jan Kurš
 */
public class SubstractOperator extends BinaryOperator {

    public SubstractOperator(Object firstOperand, Object secondOperand) {
        super(firstOperand, secondOperand);
    }

    @Override
    protected Integer evaluate(Integer val1, Integer val2) {
        return val1 - val2;
    }

    @Override
    public ExpressionElement getExpElement() {
        return new SubstractOperation();
    }
}
