package cvut.fit.dpo.arithmetic;

import java.util.Iterator;

import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import cvut.fit.dpo.arithmetic.iterator.InOrderIterator;
import cvut.fit.dpo.arithmetic.iterator.PostOrderIterator;

/**
 *
 * @author JiriKrizek
 * Builder: Product
 */
public class ArithmeticExpression {

    private Operator root;

    public Integer evaluate() {
        return root.evaluate();
    }

    public void setRoot(BinaryOperator root) {
        this.root = root;
    }

    public void setRoot(NumericOperand numeric) {
        this.root = new UnaryOperator(numeric);
    }

    /**
     * {@link #root} field getter.
     * 
     * @deprecated Do not provide access to the inner representation
     */
    public BinaryOperator getRoot() {
        throw new UnsupportedOperationException("Not supported anymore.");
    }

    public Iterator<ExpressionElement> getInOrderIterator() {
        return new InOrderIterator(root);
    }

    public Iterator<ExpressionElement> getPostOrderIterator() {
        return new PostOrderIterator(root);
    }
}
