/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package cvut.fit.dpo.arithmetic.builder;

/**
 *
 * @author Jiri Krizek
 *
 * Builder: Builder
 */
public interface ExpressionBuilder {
    abstract void buildAddOperator();
    abstract void buildSubstractOperator();
    abstract void buildNumericOperand(String str);
}
