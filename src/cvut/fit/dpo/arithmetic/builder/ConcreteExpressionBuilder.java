/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fit.dpo.arithmetic.builder;

import cvut.fit.dpo.arithmetic.AddOperator;
import cvut.fit.dpo.arithmetic.ArithmeticExpression;
import cvut.fit.dpo.arithmetic.BinaryOperator;
import cvut.fit.dpo.arithmetic.NumericOperand;
import cvut.fit.dpo.arithmetic.SubstractOperator;
import java.util.Stack;

/**
 *
 * @author Jiri Krizek
 * Builder: ConcreteBuilder
 */
public class ConcreteExpressionBuilder implements ExpressionBuilder {

    private Stack<Object> stack = new Stack<Object>();

    public ArithmeticExpression getResult() {
        final ArithmeticExpression res = new ArithmeticExpression();
        Object root = stack.peek();

        try {
            final BinaryOperator r = (BinaryOperator) root;
            res.setRoot(r);
            return res;
        } catch (ClassCastException ignore) {
        }

        try {
            final NumericOperand r = (NumericOperand) root;
            res.setRoot(r);
            return res;
        } catch (ClassCastException ignore) {
        }


        return null;
    }

    @Override
    public void buildAddOperator() {
        Object second = stack.pop();
        Object first = stack.pop();

        AddOperator add = new AddOperator(first, second);
        stack.push(add);
    }

    @Override
    public void buildSubstractOperator() {
        Object second = stack.pop();
        Object first = stack.pop();

        SubstractOperator add = new SubstractOperator(first, second);
        stack.push(add);
    }

    @Override
    public void buildNumericOperand(String str) {
        try {
            NumericOperand op = createNumericOperand(str);
            stack.push(op);
        } catch (NumberFormatException ignore) {
            throw new IllegalArgumentException();
        }
    }

    private NumericOperand createNumericOperand(String str) throws NumberFormatException {
        int num = Integer.parseInt(str);
        return new NumericOperand(num);
    }
}
