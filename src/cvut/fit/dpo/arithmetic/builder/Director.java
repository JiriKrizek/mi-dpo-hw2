package cvut.fit.dpo.arithmetic.builder;

/**
 *
 * @author Jiri Krizek
 * Builder: Director
 */
public class Director {

    private ExpressionBuilder builder;

    public Director(ExpressionBuilder b) {
        this.builder = b;
    }

    public void construct(String input) {
        String[] strings = input.split(" ");
        
        for (String str : strings) {
            if (str.equals("+")) {
                builder.buildAddOperator();
            } else if (str.equals("-")) {
                builder.buildSubstractOperator();
            } else {
                builder.buildNumericOperand(str);
            }
        }
    }
}
