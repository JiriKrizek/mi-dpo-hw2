package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.elements.CloseBracketOperation;
import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
/**
 *
 * @author JiriKrizek
 */
public class ClosedBracket extends Component {

    
    @Override
    public ExpressionElement getExpElement() {
        return new CloseBracketOperation();
    }
}
