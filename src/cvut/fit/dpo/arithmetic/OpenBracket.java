package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import cvut.fit.dpo.arithmetic.elements.OpenBracketOperation;

/**
 *
 * @author JiriKrizek
 */
public class OpenBracket extends Component {

    
    @Override
    public ExpressionElement getExpElement() {
        return new OpenBracketOperation();
    }
}
