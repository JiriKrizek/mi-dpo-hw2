package cvut.fit.dpo.arithmetic.iterator;

import cvut.fit.dpo.arithmetic.Component;
import cvut.fit.dpo.arithmetic.Operator;
import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author JiriKrizek
 * Iterator: Iterator
 * Composite: Client (v tele metody next())
 */
public abstract class MyIterator implements Iterator<ExpressionElement> {

    protected Operator root;
    protected int currentNode;
    protected final List<Component> list;

    public MyIterator(Operator root) {
        this.root = root;
        this.list = new ArrayList<Component>();
        traverse();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ExpressionElement next() {
        Component get = list.get(currentNode++);

        // Client pro vzor Composite
        return get.getExpElement();
    }

    @Override
    public boolean hasNext() {
        if (currentNode < list.size()) {
            return true;
        }

        return false;
    }
    
    public abstract void traverse();
    
    public abstract void traverse(Object node);
}
