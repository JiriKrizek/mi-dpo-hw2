package cvut.fit.dpo.arithmetic.iterator;

import cvut.fit.dpo.arithmetic.Component;
import cvut.fit.dpo.arithmetic.Operator;

/**
 *
 * @author JiriKrizek
 * Iterator: ConcreteIterator
 */
public class PostOrderIterator extends MyIterator {

    public PostOrderIterator(Operator root) {
        super(root);
    }

    @Override
    public void traverse() {
        traverse(root.getFirstOperand());
        traverse(root.getSecondOperand());
        list.add(root);
    }

    @Override
    public void traverse(Object node) {
        if (node == null) {
            return;
        }

        final Component c = (Component) node;
        traverse(c.getFirstOperand());
        traverse(c.getSecondOperand());
        list.add(c);
    }
    
    
}
