package cvut.fit.dpo.arithmetic.iterator;

import cvut.fit.dpo.arithmetic.Component;
import cvut.fit.dpo.arithmetic.Operator;

/**
 *
 * @author JiriKrizek
 * Iterator: ConcreteIterator
 */
public class InOrderIterator extends MyIterator {

    public InOrderIterator(Operator root) {
        super(root);
    }

    @Override
    public void traverse() {
        list.addAll(root.getOpBracket());

        traverse(root.getFirstOperand());
        list.add(root);
        traverse(root.getSecondOperand());

        list.addAll(root.getClBracket());
    }

    @Override
    public void traverse(Object node) {
        if (node == null) {
            return;
        }

        final Component c = (Component) node;

        list.addAll(c.getOpBracket());

        traverse(c.getFirstOperand());
        list.add(c);
        traverse(c.getSecondOperand());

        list.addAll(c.getClBracket());
    }
}
