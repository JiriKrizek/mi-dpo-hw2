package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JiriKrizek
 * Composite: Component
 */
public abstract class Component {
    
    protected Object firstOperand = null;
    protected Object secondOperand = null;

    void setFirstOperand(Object o) {
        firstOperand = o;
    }

    public Object getFirstOperand() {
        return firstOperand;
    }

    void setSecondOperand(Object o) {
        secondOperand = o;
    }

    public Object getSecondOperand() {
        return secondOperand;
    }
    
    public abstract ExpressionElement getExpElement();
    
    public List<Component> getOpBracket() {
        final List<Component> l = new ArrayList<Component>(1);
        l.add(new OpenBracket());
        return l;
    }
    
    public List<Component> getClBracket() {
        final List<Component> l = new ArrayList<Component>(1);
        l.add(new ClosedBracket());
        return l;
    }
}
