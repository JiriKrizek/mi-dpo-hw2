package cvut.fit.dpo.arithmetic;

import cvut.fit.dpo.arithmetic.elements.ExpressionElement;
import cvut.fit.dpo.arithmetic.iterator.InOrderIterator;
import cvut.fit.dpo.arithmetic.iterator.PostOrderIterator;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents number in the arithmetic expression
 * 
 * @author Jan Kurš
 * 
 * Composite: Leaf
 */
public class NumericOperand extends Component {

    private Integer value;

    public NumericOperand(Integer value) {
        setValue(value);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public InOrderIterator inOrderIterator() {
        return null;
    }

    public PostOrderIterator postOrderIterator() {
        return null;
    }

    @Override
    public ExpressionElement getExpElement() {
        return new cvut.fit.dpo.arithmetic.elements.Number(value);
    }

    public List<Component> getOpBracket() {
        return new ArrayList<Component>();
    }

    public List<Component> getClBracket() {
        return new ArrayList<Component>();
    }
}
